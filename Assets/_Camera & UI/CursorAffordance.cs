﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace RPG.CameraUI
    { 
    [RequireComponent(typeof(CameraRaycaster))]
    public class CursorAffordance : MonoBehaviour
    {
        [SerializeField] Texture2D walkCursor = null;
        [SerializeField] Texture2D attackCursor = null;
        [SerializeField] Texture2D unknownCursor = null;
        [SerializeField] Texture2D cursor = null;

        [SerializeField] Vector2 cursorHotSpot = new Vector2(96, 96);

        //TODO
        [SerializeField] const int walkableLayerNumber = 9;
        [SerializeField] const int enemyLayerNumber = 10;
        //[SerializeField] const int stiffLayerNumber = 11;

        public CameraRaycaster cameraRaycaster;

        public CursorMode cursorMode = CursorMode.Auto; 

        // Start is called before the first frame update
        void Start()
        {
            cameraRaycaster = GetComponent<CameraRaycaster>();
            cameraRaycaster.notifyLayerChangeObservers += OnLayerChange;
        }

        // Update is called once per frame
        void OnLayerChange(int newLayer)
        { 
            switch (newLayer)
            {
                case walkableLayerNumber:
                    cursor = walkCursor;
                    break;
                case enemyLayerNumber:
                    cursor = attackCursor; 
                    break;
                default:
                    cursor = unknownCursor; 
                    break;

            } 
            Cursor.SetCursor(cursor, cursorHotSpot, cursorMode);
            //   print(cameraRaycaster.currentLayerHit);
        }


        // TODO de-registering (destroy) OnLayerChange
    }
}